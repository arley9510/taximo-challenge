import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class SynchronousShopping {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    parameters: string;

    @Column()
    shoping_centers: string;

    @Column()
    roads: string;

    @Column()
    minimum_time: number;
}
