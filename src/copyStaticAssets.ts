import * as shell from 'shelljs';

shell.cp('-R', __dirname + '/public/', 'dist/public/');
shell.cp('-R', __dirname + '/templates/', 'dist/templates/');