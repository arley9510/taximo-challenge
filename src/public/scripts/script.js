function testUpload() {
    const input = document.getElementById("input-taximo").value;
    input.trim();

    if (input !== '') {
        fetch('http://taximochallenge-env.us-east-2.elasticbeanstalk.com/api/v1/synchronous_shopping/raw', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                'data': input.trim()
            })
        }).then((response) => {
            if (response.status === 200) {
                return response.json();
            } else {
                window.alert(response.statusText);
            }
        }).then((responseJson) => {
            if (typeof responseJson !== 'undefined') {
                document.getElementById("taximo-response").value = responseJson.minimum_time;
            }
        }).catch((error) => {
            console.error(error);
        })
    }
}
