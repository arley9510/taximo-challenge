import { BaseContext } from 'koa';

export default class GeneralController {

    public static async challengeView(ctx: BaseContext) {
        const options = {
            author: 'Arley David Fernandez Cervantes',
            title: 'Taximo challenge',
        };

        return ctx.render('main', {options: options});
    }
}
