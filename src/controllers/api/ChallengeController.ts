import { BaseContext } from 'koa';
import { getManager, Repository } from 'typeorm';
import { validate, ValidationError } from 'class-validator';

import { User } from '../../entities/User';
import { SynchronousShopping } from '../../entities/SynchronousShopping';
import SynchronousShoppingController from '../base/SynchronousShoppingController';
import { TaximoChallengeInterface } from '../../interfaces/TaximoChallengeInterface';

export default class ChallengeController {
    /**
     *
     * @param ctx
     */
    public static async challenge(ctx: BaseContext) {
        const data: TaximoChallengeInterface = ctx.query;

        await ChallengeController.validateQueryData(data, ctx);

        await ChallengeController.validateSynchronousShopping(data, ctx)
            .then(async (response) => {
                if (response.toString().length > 0) {
                    ctx.body = response.shift();
                } else {
                    const parseData = SynchronousShoppingController.formattingData(data);
                    const minimumTime = await SynchronousShoppingController.minTime(parseData);

                    ChallengeController.saveResult(data, minimumTime).then();

                    ctx.body = {minimum_time: minimumTime};
                }
                ctx.status = 200;
            });
    }

    /**
     *
     * @param {Application.BaseContext} ctx
     * @returns {Promise<void>}
     */
    public static async challengeRaw(ctx: BaseContext) {
        const body: any = ctx.request.body;

        const data = body['data'];

        await ChallengeController.validateRaw(data, ctx);

        const respponse = await SynchronousShoppingController.minTime(data);


        ctx.body = {minimum_time: respponse};
    }

    /**
     *
     * @param {string} data
     * @param {Application.BaseContext} ctx
     * @returns {Promise<void>}
     */
    private static async validateRaw(data: string, ctx: BaseContext) {
        data.split(' ').forEach((element) => {
            if (!parseInt(element)) {
                return ChallengeController.errorDataValidation(ctx, '');
            }
        });
    }

    /**
     *
     * @param {TaximoChallengeInterface} data
     * @param {Application.BaseContext} ctx
     * @returns {Promise<void>}
     */
    private static async validateQueryData(queryData: TaximoChallengeInterface, ctx: BaseContext) {
        const UserRepository: Repository<User> = getManager().getRepository(User);
        const data: TaximoChallengeInterface = {
            username: queryData.username,
            parameters: queryData.parameters,
            shoping_centers: queryData.shoping_centers,
            roads: queryData.roads,
            checksum: queryData.checksum
        };

        const errors: ValidationError[] = await validate(data);

        if (errors.toString().length > 0) {
            return ChallengeController.errorDataValidation(ctx, '');
        }

        const user = await UserRepository.findOne({name: data.username});

        if (typeof user === 'undefined') {
            this.errorDataValidation(ctx, 'username');
        }

        if (user.hash !== data.checksum) {
            this.errorDataValidation(ctx, 'checksum');
        }

        Object.keys(data).forEach((response) => {
            ChallengeController.queryParamValidator(data[response], response, ctx);
        });

        ['parameters', 'shoping_centers', 'roads'].forEach((response) => {
            ChallengeController.validateNumbersInData(data[response], ctx);
        });
    }

    /**
     *
     * @param data
     * @param ctx
     */
    private static async validateSynchronousShopping(data: TaximoChallengeInterface, ctx: BaseContext) {
        return await getManager()
            .query(
                'SELECT minimum_time FROM "public"."synchronous_shopping" WHERE ' +
                'parameters = $1 AND shoping_centers = $2 AND roads = $3'
                , [
                    data.parameters,
                    data.shoping_centers,
                    data.roads
                ]
            );
    }

    /**
     *
     * @param data
     * @param time
     */
    private static async saveResult(data: TaximoChallengeInterface, time: number) {
        const SynchronousShoppingRepository: Repository<SynchronousShopping> = getManager()
            .getRepository(SynchronousShopping);

        const dataToBeSaved: SynchronousShopping = new SynchronousShopping();

        dataToBeSaved.parameters = data.parameters;
        dataToBeSaved.shoping_centers = data.shoping_centers;
        dataToBeSaved.roads = data.roads;
        dataToBeSaved.minimum_time = time;

        const errors: ValidationError[] = await validate(dataToBeSaved);

        if (errors.length === 0) {
            await SynchronousShoppingRepository.save(dataToBeSaved);
        }
    }

    private static queryParamValidator(param, name, ctx: BaseContext) {
        if ((param === null || param === ''))
            return ChallengeController.errorDataValidation(ctx, name);
    }

    /**
     *
     * @param data
     * @param ctx
     */
    private static validateNumbersInData(data: string, ctx: BaseContext) {
        data.split(',').forEach((response) => {
            if (!parseInt(response))
                ChallengeController.errorDataValidation(ctx, '');
        });
    }

    /**
     *
     * @param ctx
     * @param message
     */
    private static errorDataValidation(ctx: BaseContext, message: string) {
        const errorText = message !== '' ?
            'Data validation failed, please check the following parameter: ' + message :
            'The request is invalid';

        ctx.throw(422, errorText);
    }
}
