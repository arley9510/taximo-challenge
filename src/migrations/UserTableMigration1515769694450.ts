import { MigrationInterface, QueryRunner } from 'typeorm';

export class UserTableMigration1515769694450 implements MigrationInterface {

    async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(
            `INSERT INTO "public"."user" ("name", "hash") VALUES('taximo_api_user', 'cd7ced88fb72ee862940d5664555251f9ba044d8478a71a7b70b04bd708c2796')`
        );
    }

    async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`DELETE FROM "public"."user" WHERE name="taximo_api_user"`);
    }

}
