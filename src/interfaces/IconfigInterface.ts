export interface IConfig {
    port: number;
    debugLogging: boolean;
    dbsslconn: boolean;
    host: string;
    dbPort: number;
    username: string;
    password: string;
    database: string;
}
