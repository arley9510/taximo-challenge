import 'reflect-metadata';
import * as Koa from 'koa';
import * as path from 'path';
import * as dotenv from 'dotenv';
import * as cors from '@koa/cors';
import * as winston from 'winston';
import * as views from 'koa-views';
import * as serve from 'koa-static';
import * as helmet from 'koa-helmet';
import { createConnection } from 'typeorm';
import * as bodyParser from 'koa-bodyparser';

import { config } from './config';
import { router } from './routes';
import { logger } from './logging';

// Load environment variables from .env file, where API keys and passwords are configured
dotenv.config();

// note that its not active database connection
// TypeORM creates you connection pull to uses connections from pull on your requests
createConnection({
    type: 'postgres',
    host: config.host,
    port: config.dbPort,
    username: config.username,
    password: config.password,
    database: config.database,
    synchronize: true,
    logging: false,
    entities: [
        'src/entities/**/*.ts'
    ],
    migrationsRun: true,
    migrations: [
        'src/migrations/**/*.ts'
    ],
    'cli': {
        'migrationsDir': 'src/migrations/**/*.ts'
    },
    extra: {
        ssl: config.dbsslconn, // if not development, will use SSL
    }
}).then(async () => {

    const app = new Koa();

    // Provides important security headers to make your app more secure
    app.use(helmet());

    // Enable cors with default options
    app.use(cors());

    // Logger middleware -> use winston as logger (logging.ts with config)
    app.use(logger(winston));

    // Enable bodyParser with default options
    app.use(bodyParser());

    app.use(serve('.'));

    app.use(serve('/app/dist/public'));

    app.use(views(path.join('/app/dist/templates'), {extension: 'ejs'}));

    // this provides the routes
    app.use(router.routes()).use(router.allowedMethods());

    // start the app
    app.listen(config.port);

    console.log(`Server running on port ${config.port}`);

}).catch(error => console.log('TypeORM connection error: ', error));
