export interface TaximoChallengeInterface {
    username: string;
    parameters: string;
    shoping_centers: string;
    roads: string;
    checksum: string;
}
