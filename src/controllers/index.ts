export { default as general } from './web/GeneralController';
export { default as challenge } from './api/ChallengeController';