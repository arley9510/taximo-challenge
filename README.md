# Taximo challenge

In this file I'm gonna explain the architecture and the implementation of my software solution.

The application was made in the node js and TypeScript in the compilation process using koa js framework as the http manager. In the build process was used Docker
implementing the AWS Elastic Beanstalk for deploy the app in the web server with bitbucket repository for version managment use git command line and bitbucket pipelines for run the funtional and integration tests.

## Development tools

* [node js](https://nodejs.org/es/)
* [npm](https://www.npmjs.com/)
* [Koa js](https://koajs.com/)
* [TypeScript](https://www.typescriptlang.org/)
* [docker](https://www.docker.com/)
* [docker compose](https://docs.docker.com/compose/overview/)
* [jest](https://jestjs.io/)
* [sonarQube](https://www.sonarqube.org/)
* [EB CLI](https://docs.aws.amazon.com/es_es/elasticbeanstalk/latest/dg/eb-cli3.html)
* [Bitbucket Pipelines ](https://bitbucket.org/product/features/pipelines)
* [git](https://git-scm.com/)

## File structure
```
project
├── __tests__
│            └─── unit
│                     └─── controllers
│                                    │   ChallengeController.spec.ts
│                                    │   GeneralController.spec.ts
│                                    │   SynchronousShoppingController.spec.ts
│                                    └─── 
├── src
│      │
│      ├── controllers
│      │              │
│      │              ├── api
│      │              │      │  ChallengeController.ts
│      │              │      └───
│      │              ├── base
│      │              │       │ SynchronousShoppingController.ts
│      │              │       └───
│      │              ├── web
│      │              │      │  GeneralController.ts
│      │              │      └───
│      │              │  index.ts
│      │              └───
│      │ 
│      ├── entities
│      │           │  SynchronousShopping.ts
│      │           │  User.ts
│      │           └───
│      │
│      ├── interfaces
│      │             │  IconfigInterface.ts
│      │             │  TaximoChallengeInterface.ts
│      │             └───
│      │
│      ├── migrations
│      │             │  UserTableMigration1515769694450.ts
│      │             └───
│      │
│      ├── public
│      │         ├── images
│      │         │          │   favicon.ico
│      │         │          └─── 
│      │         ├── scripts
│      │         │          │   script.js
│      │         │          └─── 
│      │         └─── styles
│      │                    │    style.css
│      │                    └───
│      │        
│      ├── templates
│      │            ├── common
│      │            │         │  footer.ejs
│      │            │         │  head.ejs
│      │            │         └───
│      │            │  error.ejs      
│      │            │  main.ejs
│      │            └───
│      │
│      │   config.ts
│      │   copyStaticAssets.ts
│      │   logging.ts
│      │   routes.ts
│      │   server.ts
│      └───
│   .dockerignore
│   .gitignore
│   bitbucket-pipelines.yml
│   docker-compose.yml
│   Dockerfile
│   Dockerrun.aws.json
│   fullstack_challenge.txt
│   jest.config.js
│   package.json
│   package-lock.json
│   README.md
│   sonar-project.properties
│   tsconfig.json
│   tslint.json
└───
```

## Project layers

* \_\_tests__ --> **Folder containing the test files.**
    * unit
    
        * controller
        
            * ChallengeController.spec.ts --> **Test file for challenge controller.**
            * GeneralController.spec.ts --> **Test file for general controller.**
            * SynchronousShoppingController.spec.ts --> **Test file for synchronous shopping controller.**
            
* src --> **Folder containing project logic, views and server run file.**

    * controllers --> **Folder containing pure logic controllers.**
    
        * api
        
            * ChallengeController.ts --> **File containing logic for the end point for solve request.**
        * base
        
            * SynchronousShoppingController.ts --> **File containing logic for solve the challenge.**
            
        * web
        
            * GeneralController.ts --> **File containing the logic for render challenge web view.**

* entities --> **Folder containing models for data persistence in the postgresSQL database.**
    
    * SynchronousShopping.ts --> **File containing the structure of the table that saves the results.**
    
    * User.ts --> **File containing the structure of the table user.**

* interfaces
    
    * IconfigInterface.ts --> **File containing the model rules for the database connection.**
    * TaximoChallengeInterface.ts --> **File containing the model rules for the query params of the challenge.**

* migrations --> **Folder containing the migrations the seeders for th database.**
 
    * --> UserTableMigration1515769694450.ts **File containing the migration for user table.**
    
* public --> **Folder containing the public files of the node js server.**
    
    * images
        * favico.ico
        
    * scrips
        * script.js
       
    * styles
        * style.css

* templates --> **Folder containing the web view files ofr node js render process.**

    * common
        
        * footer.ejs
        
        * head.ejs
            
    * error.ejs
    
    * main.ejs
    
## Project classes
 
 * src/controllers/api**ChallengeController.ts**
    
    - public challenge
    - public challengeRaw
    - public validateRaw
    - public validateQueryData
    - public validateSynchronousShopping
    - public saveResult
    - public queryParamValidator
    - public validateNumbersInData
    - public errorDataValidation
 
 * src/controllers/base/**SynchronousShoppingController.ts**
    
    - minTime
    - formattingData
    - splitString
 
 * src/controllers/web**GeneralController.ts**
    
    - challengeView

## Data model

### user
| id         | name       | hash       | 
| ---------- | ---------- | ---------- | 
| number     | string     | string     |

### synchronousShopping
| id         | parameters | shoping_centers |  roads     | minimum_time |
| ---------- | ---------- | --------------- | ---------- | ------------ |
| number     | string     | string          | string     |   string     |

## Project implementation

* clone the project whit this command 

```
    git clone https://arley9510@bitbucket.org/arley9510/taximo-challenge.git
```

* install libraries and dependencies in the project folder in a terminal

```
    npm install
```
 
* Run the project whit docker in the project folder in a terminal

```
    docker-compose up
```
 
* Now the project up in the port 3000

```
    localhost:3000/
```
 
* For test the following command in the folder project on a terminal

```
    npm test
```

## Code quality

* For the quality of the code in the project we use SonarQube in the Docker environment

![](https://docs.sonarqube.org/download/attachments/11639983/image2017-10-31%2013%3A9%3A10.png?version=1&modificationDate=1509451751000&api=v2)

* To test sonarQube You must start a Docker container with the image of SonarQube

    - user: admin
    - password: admin

```
    docker run -d --name sonarqube -p 9000:9000 -p 9092:9092 sonarqube
```

* Must be install the Sonar Scanner plugin to be able to run SonarQube from [here](https://docs.sonarqube.org/display/SCAN/Analyzing+with+SonarQube+Scanner#AnalyzingwithSonarQubeScanner-Installation)

* Once the plugin is installed run this command in the project folder on a terminal

```
    sonar-scanner
```

