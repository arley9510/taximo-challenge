import * as dotenv from 'dotenv';

import { IConfig } from './interfaces/IconfigInterface';

dotenv.config();

const config: IConfig = {
    port: parseInt(process.env.PORT) || 3000,
    debugLogging: process.env.NODE_ENV === 'development',
    dbsslconn: process.env.NODE_ENV !== 'development',
    host: process.env.NODE_ENV === 'production' ? process.env.RDS_HOSTNAME : process.env.POSTGRES_HOST,
    dbPort: process.env.NODE_ENV === 'production' ? parseInt(process.env.RDS_PORT) : 5432,
    username: process.env.NODE_ENV === 'production' ? process.env.RDS_USERNAME : process.env.POSTGRES_USER,
    password: process.env.NODE_ENV === 'production' ? process.env.RDS_PASSWORD :  process.env.POSTGRES_PASSWORD,
    database: process.env.NODE_ENV === 'production' ? process.env.RDS_DB_NAME : process.env.POSTGRES_DB
};

export { config };
