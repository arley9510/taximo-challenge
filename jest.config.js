module.exports = {
    "reporters": [
        "default",
        ["jest-junit", {
            "output": "./test-reports/junit.xml",
            "classNameTemplate": "{classname}-{title}",
            "titleTemplate": "{classname}-{title}",
            "ancestorSeparator": " › ",
            "usePathForSuiteName": "true"
            }
        ]
    ],
    "transform": {
        "^.+\\.tsx?$": "ts-jest"
    },
    "testRegex": "(/__tests__/.*|(\\.|/)(test|spec))\\.tsx?$",
    "moduleFileExtensions": [
        "ts",
        "tsx",
        "js",
        "jsx",
        "json",
        "node"
    ],
};
