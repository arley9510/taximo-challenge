import ChallengeController from '../../../src/controllers/api/ChallengeController';

test('test controller build', async () => {
    const controller = new ChallengeController();

    expect(controller).toBe(controller);
});

test('test challenger raw', async () => {
    const ctx = {
        request: {
            body: {
                data: '6 10 3\n' +
                    '2 1 2\n' +
                    '1 3\n' +
                    '0\n' +
                    '2 1 3\n' +
                    '1 2\n' +
                    '1 3\n' +
                    '1 2 572\n' +
                    '4 2 913\n' +
                    '2 6 220\n' +
                    '1 3 579\n' +
                    '2 3 808\n' +
                    '5 3 298\n' +
                    '6 1 927\n' +
                    '4 5 171\n' +
                    '1 5 671\n' +
                    '2 5 463'
            }
        }
    };

    await ChallengeController.challengeRaw(ctx).then();

    expect(ctx.body).toEqual({minimum_time: 792});
});
