import * as Router from 'koa-router';
import controller = require('./controllers');

const router = new Router();

// GENERAL ROUTES
router.get('/', controller.general.challengeView);

// CHALLENGE ROUTES
router.get('/api/v1/synchronous_shopping', controller.challenge.challenge);
router.post('/api/v1/synchronous_shopping/raw', controller.challenge.challengeRaw);

export { router };