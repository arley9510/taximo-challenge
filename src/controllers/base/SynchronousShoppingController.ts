import { TaximoChallengeInterface } from '../../interfaces/TaximoChallengeInterface';

export default class SynchronousShoppingController {

    public static async minTime(data: string) {
        const INF = 20000000;
        let inputDataIndex = 0;
        let input_data: any = data;
        let mask: any, n: any, m: any, k: any, dist: any, heap: any, adj: any, indexTable: any;

        function readLine() {
            return (inputDataIndex >= input_data.length) ? undefined : input_data[inputDataIndex++];
        }

        function siftUp(index) {
            while (index > 0) {
                const prev = ((index - 1) / 2) >> 0;

                if (dist[heap[prev].v][heap[prev].m] <= dist[heap[index].v][heap[index].m])
                    break;

                const tmp = heap[prev];
                heap[prev] = heap[index];
                heap[index] = tmp;

                indexTable[heap[prev].v][heap[prev].m] = prev;
                indexTable[heap[index].v][heap[index].m] = index;

                index = prev;
            }
        }

        function siftDown(index) {
            while (index < heap.length) {
                let smallest = index;
                let next = 2 * index + 1;

                if (next < heap.length && dist[heap[next].v][heap[next].m] < dist[heap[smallest].v][heap[smallest].m])
                    smallest = next;

                next++;

                if (next < heap.length && dist[heap[next].v][heap[next].m] < dist[heap[smallest].v][heap[smallest].m])
                    smallest = next;

                if (smallest == index)
                    break;

                const tmp = heap[index];

                heap[index] = heap[smallest];
                heap[smallest] = tmp;

                indexTable[heap[index].v][heap[index].m] = index;
                indexTable[heap[smallest].v][heap[smallest].m] = smallest;

                index = smallest;
            }
        }

        function resolve() {
            input_data = input_data.split('\n');
            let line = readLine().split(' ').map(Number);
            n = line[0];
            m = line[1];
            k = line[2];

            mask = [];
            mask.length = n;

            adj = [];
            adj.length = n;

            heap = [];
            dist = [];
            dist.length = n;
            indexTable = [];
            indexTable.length = n;

            for (let i = 0; i < n; ++i) {
                dist[i] = [];
                indexTable[i] = [];

                for (let j = 0; j < (1 << k); ++j) {
                    dist[i][j] = INF;
                    indexTable[i][j] = -1;
                }
            }

            for (let i = 0; i < n; ++i) {
                adj[i] = [];
                mask[i] = 0;
                line = readLine().split(' ').map(Number);
                for (let j = 1; j < line.length; ++j) {
                    line[j]--;
                    mask[i] |= (1 << line[j]);
                }
            }

            for (let i = 0; i < m; ++i) {
                line = readLine().split(' ').map(Number);
                const x = line[0] - 1;
                const y = line[1] - 1;

                adj[x].push({v: y, w: line[2]});
                adj[y].push({v: x, w: line[2]});
            }

            indexTable[0][mask[0]] = 0;
            dist[0][mask[0]] = 0;
            heap.push({v: 0, m: mask[0]});

            while (heap.length > 0) {
                const curElem = heap[0];
                heap[0] = heap[heap.length - 1];
                indexTable[heap[0].v][heap[0].m] = 0;
                indexTable[curElem.v][curElem.m] = -1;
                heap.pop();
                siftDown(0);

                for (let i = 0; i < adj[curElem.v].length; ++i) {
                    const next = adj[curElem.v][i].v;
                    const nextMask = curElem.m | mask[next];
                    const weight = adj[curElem.v][i].w;

                    if (dist[next][nextMask] > dist[curElem.v][curElem.m] + weight) {
                        dist[next][nextMask] = dist[curElem.v][curElem.m] + weight;
                        if (indexTable[next][nextMask] == -1) {
                            heap.push({v: next, m: nextMask});
                            indexTable[next][nextMask] = heap.length - 1;
                        }

                        siftUp(indexTable[next][nextMask]);
                    }
                }
            }

            let ans = INF;
            for (let i = 0; i < (1 << k); ++i) {
                for (let j = i + 1; j < (1 << k); ++j) {
                    if ((i | j) != (1 << k) - 1)
                        continue;

                    const m = Math.max(dist[n - 1][i], dist[n - 1][j]);

                    ans = Math.min(ans, m);
                }
            }

            return ((ans === INF) ? dist[n - 1][(1 << k) - 1] : ans);
        }

        return resolve();
    }

    public static formattingData(data: TaximoChallengeInterface) {
        let firstStep: string = '';
        let secondStep: string = '';

        firstStep = SynchronousShoppingController.splitString(',', ' ', data.parameters, firstStep);
        firstStep += '\n';
        firstStep = SynchronousShoppingController.splitString(',', ' ', data.shoping_centers, firstStep);
        firstStep += '\n';
        firstStep = SynchronousShoppingController.splitString(',', ' ', data.roads, firstStep);

        secondStep = SynchronousShoppingController.splitString('-', '\n', firstStep, secondStep);

        return secondStep.trimRight();
    }

    public static splitString(symbol: string, splitSymbol: string, string: string, container: string): string {
        string.split(symbol).forEach((response) => {
            container += response + splitSymbol;
        });

        return container.trim();
    }
}
