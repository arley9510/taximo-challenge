FROM mhart/alpine-node:8.12.0

MAINTAINER Arley David Fernandez Cervantes

RUN mkdir /app

WORKDIR /app

COPY . /app

RUN npm install

RUN npm install -g ts-node typescript

EXPOSE 3000

CMD ["sh", "-c", "npm rum build-ts && npm run copy-static-assets && npm run serve"]
